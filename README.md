A simple library that can zip folder with it content.

Based on user comment from:

http://php.net/manual/de/class.ziparchive.php


usage:

```
HZip::zipDir($sourcePath, $outZipPath);
```


Requirements:

Requires pecl zip extension above 1.0
